defmodule GameTest do
  use ExUnit.Case
  doctest Game

  alias Game.{Towers, Instruction}

  test "perform an valid action" do
    input = Towers.from([[1, 2, 3], [], []])

    action = Instruction.create(0, 2)

    expected = Towers.from([[2, 3], [], [1]])

    assert {:ok, expected} == Game.perform(input, action)
  end

  test "perform an invalid action - empty tower" do
    input = Towers.from([[1, 2, 3], [], []])

    action = Instruction.create(1, 2)

    assert {:error, "tower is empty", input} == Game.perform(input, action)
  end

  test "perform an invalid action - invalid disk size" do
    input = Towers.from([[2, 3], [], [1]])

    action = Instruction.create(0, 2)

    assert {:error, "disk size must be smaller", input} == Game.perform(input, action)
  end

  test "perform multiple actions" do
    input = Towers.from([[1, 2, 3], [], []])

    actions = [
      Instruction.create(0, 2),
      Instruction.create(0, 1),
      Instruction.create(2, 1)
    ]

    expected = Towers.from([
      [3],
      [1, 2],
      []
    ])

    assert {:ok, expected} == Game.perform(input, actions)
  end
end
