defmodule Game do
  alias Game.{Tower, Towers, Instruction, Validator}


  def perform(
    %Towers{} = towers,
    []
  ) do
    {:ok, towers}
  end
  def perform(
    %Towers{} = towers,
    [%Instruction{} = instruction | instructions] \\ []
  ) do
    towers
    |> perform(instruction)
    |> case do
      {:ok, towers} -> perform(towers, instructions)
      {:error, reason, towers} -> {:error, reason, towers}
    end
  end

  def perform(
    %Towers{} = towers,
    %Instruction{from: from, to: to}
  ) do
    with  {:ok, source} <- Towers.get(towers, from),
          {:ok, destination} <- Towers.get(towers, to),
          {:ok, disk, updated_source} <- Tower.pop(source),
          {:ok, updated_destination} <- Tower.push(destination, disk), 
          true <- Validator.is_valid?(updated_destination) do
      next_towers =    
        towers
        |> Towers.update(from, updated_source)
        |> Towers.update(to, updated_destination)

      {:ok, next_towers}
    else 
      {:error, reason, _tower} -> {:error, reason, towers}
      false -> {:error, "disk size must be smaller", towers}
    end
  end
end
