defmodule Game.Instruction do
  
  @enforce_keys [:from, :to]

  defstruct [:from, :to]

  @doc """
  ## Examples
  iex> Game.Instruction.create(0, 2)
  %Game.Instruction{from: 0, to: 2}

  iex> Game.Instruction.create("0", 2)
  ** (FunctionClauseError) no function clause matching in Game.Instruction.create/2
  """

  def create(from, to) 
    when is_integer(from)
    and is_integer(to) do
    %__MODULE__{from: from, to: to}
  end
end