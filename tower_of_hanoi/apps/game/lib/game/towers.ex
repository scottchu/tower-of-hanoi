defmodule Game.Towers do
  alias Game.Tower

  @enforce_keys [:towers]

  defstruct [
    towers: []
  ]

  @type t() :: %__MODULE__{
    towers: [Tower.t()]
  }

  @doc """
  ## Examples
  iex> Game.Towers.from([[1, 2, 3], [4, 5, 6]])
  %Game.Towers{
    towers: [
      %Game.Tower{
        disks: [
          %Game.Disk{size: 1},
          %Game.Disk{size: 2},
          %Game.Disk{size: 3},
        ]
      },
      %Game.Tower{
        disks: [
          %Game.Disk{size: 4},
          %Game.Disk{size: 5},
          %Game.Disk{size: 6},
        ]
      }
    ]
  }
  """
  def from(raw_input) do
    raw_input
    |> Enum.map(&Tower.from/1)
    |> create()
  end

  @doc """
  ## Examples
  iex> Game.Towers.create([
  ...>  Game.Tower.create([
  ...>  Game.Disk.create(1)
  ...>  ]),
  ...> Game.Tower.create()
  ...> ])
  %Game.Towers{
    towers: [
      %Game.Tower{
        disks: [
          %Game.Disk{size: 1}
        ]
      },
      %Game.Tower{
        disks: []
      }
    ]
  }
  """
  @spec create(towers :: [Tower.t()]) :: t()
  def create(towers \\ []) do
    %__MODULE__{towers: towers}
  end

  @doc """
  ## Examples
  iex> towers = Game.Towers.create([
  ...>   Game.Tower.create([Game.Disk.create(1)]),
  ...>   Game.Tower.create([]),
  ...>   Game.Tower.create([])
  ...> ])
  iex> Game.Towers.get(towers, 0)
  {:ok, %Game.Tower{disks: [%Game.Disk{size: 1}]}}
  iex> Game.Towers.get(towers, 4)
  {:error, "unable to find tower at 4"}
  """
  @spec get(towers :: t(), index :: integer()) 
    :: {:ok, Tower.t()}
    |  {:error, term()}
  def get(%__MODULE__{towers: towers},  index) 
    when is_integer(index) do
    towers
    |> Enum.at(index)
    |> case do
      %Tower{} = tower -> {:ok, tower}
      nil -> {:error, "unable to find tower at #{index}"}
    end
  end

  @spec update(t(), integer(), Tower.t()) :: t()
  def update(%__MODULE__{towers: towers}, index, %Tower{} = tower) do
    towers
    |> List.replace_at(index, tower)
    |> create()
  end

  defimpl Game.Validator do
    def is_valid?(%Game.Towers{towers: towers}) do
      towers
      |> Enum.all?(&Game.Validator.is_valid?/1)
    end
  end

  defimpl Game.RawData do
    def raw_data(%Game.Towers{towers: towers}) do
      towers
      |> Enum.map(&Game.RawData.raw_data/1)
    end
  end
end