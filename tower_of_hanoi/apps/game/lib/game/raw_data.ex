defprotocol Game.RawData do

  @spec raw_data(record :: t()) :: term()
  def raw_data(record)

end

defimpl Game.RawData, for: Any do
  def raw_data(_), do: nil
end