defmodule Game.Tower do
  alias Game.Disk

  @enforce_keys [:disks]

  defstruct [
    disks: []
  ]

  @type t() :: %__MODULE__{
    disks: [Disk.t()]
  }

  @doc """
  ## Examples
  iex> Game.Tower.from([1, 2, 3])
  %Game.Tower{
    disks: [
      %Game.Disk{size: 1},
      %Game.Disk{size: 2},
      %Game.Disk{size: 3},
    ]
  }
  """
  def from(raw_input) when is_list(raw_input) do
    raw_input
    |> Enum.map(&Disk.create/1)
    |> create()
  end

  @doc """
  Creates a tower with given disks
  ## Examples
  iex> Game.Tower.create()
  %Game.Tower{disks: []}

  iex> disks = [Game.Disk.create(1)]
  iex> Game.Tower.create(disks)
  %Game.Tower{
    disks: [
      %Game.Disk{size: 1}
    ]
  }

  iex> disks = [Game.Disk.create(1), Game.Disk.create(2)]
  iex> Game.Tower.create(disks)
  %Game.Tower{
    disks: [
      %Game.Disk{size: 1},
      %Game.Disk{size: 2}
    ]
  }
  """
  @spec create(disks :: [Disk.t()]) :: t()
  def create(disks \\ []) do
    %__MODULE__{disks: disks}
  end

  @doc """
  Pop the first disk from the tower
  ## Examples
  iex> tower = Game.Tower.create([
  ...> Game.Disk.create(1),
  ...> Game.Disk.create(2),
  ...> Game.Disk.create(3)
  ...> ])
  iex> Game.Tower.pop(tower)
  {
    :ok,
    %Game.Disk{size: 1},
    %Game.Tower{
      disks: [
        %Game.Disk{size: 2},
        %Game.Disk{size: 3}
      ]
    }
  }

  iex> tower = Game.Tower.create([])
  iex> Game.Tower.pop(tower)
  {
    :error,
    "tower is empty",
    %Game.Tower{
      disks: []
    }
  }
  """
  @spec pop(Game.Tower.t())
    :: {:error, term(), t()}
    |  {:ok, Disk.t(), t()}
  def pop(%__MODULE__{disks: disks} = tower) do
    disks
    |> List.pop_at(0)
    |> case do
      {nil, []} -> {:error, "tower is empty", tower}
      {disk, disks} -> {:ok, disk, create(disks)}
    end
  end

  @doc """
  ## Examples
  ## Examples
  iex> tower = Game.Tower.create([
  ...> Game.Disk.create(2),
  ...> Game.Disk.create(3)
  ...> ])
  iex> disk = Game.Disk.create(1)
  iex> Game.Tower.push(tower, disk)
  {
    :ok,
    %Game.Tower{
      disks: [
        %Game.Disk{size: 1},
        %Game.Disk{size: 2},
        %Game.Disk{size: 3}
      ]
    }
  }
  """
  @spec push(t(), Disk.t()) :: {:ok, t()}
  def push(%__MODULE__{disks: disks}, %Disk{} = disk) do
    tower = 
      disks
      |> List.insert_at(0, disk)
      |> create()

    {:ok, tower}
  end

  defimpl Game.Validator  do
    def is_valid?(%Game.Tower{disks: disks}) do
      disks
      |> _is_ordered_properly?()
    end

    defp _is_ordered_properly?(validity \\ true, _disks)
    defp _is_ordered_properly?(validity, []), do: validity
    defp _is_ordered_properly?(validity, [_]), do: validity
    defp _is_ordered_properly?(validity, [lhs | [rhs | _] = remaining]) do
     lhs
     |> Disk.is_smaller?(rhs)
     |> Kernel.and(validity)
     |> _is_ordered_properly?(remaining)
    end
  end

  defimpl Game.RawData do
    def raw_data(%Game.Tower{disks: disks}) do
      disks
      |> Enum.map(&Game.RawData.raw_data/1)
    end
  end
end
