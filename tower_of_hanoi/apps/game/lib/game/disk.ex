defmodule Game.Disk do

  @enforce_keys [:size]

  defstruct [:size]

  @type t() :: %__MODULE__{
    size: integer()
  }

  @doc """
  ## Examples
  iex> Game.Disk.create(3)
  %Game.Disk{size: 3}

  iex> Game.Disk.create("a")
  ** (FunctionClauseError) no function clause matching in Game.Disk.create/1

  iex> Game.Disk.create()
  ** (UndefinedFunctionError) function Game.Disk.create/0 is undefined or private
  """
  @spec create(size :: integer()) :: t()
  def create(size) when is_integer(size) do
    %__MODULE__{size: size}
  end

  def is_smaller?(%__MODULE__{size: lhs}, %__MODULE__{size: rhs}) do
    lhs < rhs
  end

  defimpl Game.Validator  do
    def is_valid?(%Game.Disk{} = _disk), do: true
  end

  defimpl Game.RawData do
    def raw_data(%Game.Disk{size: size}), do: size
  end
end
