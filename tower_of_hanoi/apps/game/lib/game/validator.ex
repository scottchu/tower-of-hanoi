defprotocol Game.Validator do
  
  @doc """
    ## Example
    iex> tower = Game.Tower.from([])
    iex> Game.Validator.is_valid?(tower)
    true

    iex> tower = Game.Tower.from([1, 2, 3])
    iex> Game.Validator.is_valid?(tower)
    true

    iex> tower = Game.Tower.from([1, 3, 2])
    iex> Game.Validator.is_valid?(tower)
    false

    iex> towers = Game.Towers.from([[1, 2, 3]])
    iex> Game.Validator.is_valid?(towers)
    true

    iex> towers = Game.Towers.from([[1, 3, 2]])
    iex> Game.Validator.is_valid?(towers)
    false

    iex> towers = Game.Towers.from([[4, 5, 6], [1, 3, 2]])
    iex> Game.Validator.is_valid?(towers)
    false

    iex> Game.Validator.is_valid?(1)
    false

    iex> Game.Validator.is_valid?("a")
    false
  """
  @spec is_valid?(t()) :: boolean()
  @fallback_to_any true
  def is_valid?(candidate)
end

defimpl Game.Validator, for: Any do
  def is_valid?(_), do: false
end