# Tower of Hanoi 

## History
Tower of Hanoi game is a puzzle invented by French mathematician Êdouard Lucas in 1883.

History of Tower of Hanoi
There is a story about an ancient temple in India (Some say it’s in Vietnam – hence the name Hanoi) has a large room with three towers surrounded by 64 golden disks.

These disks are continuously moved by priests in the temple. According to a prophecy, when the last move of the puzzle is completed the world will end.

These priests acting on the prophecy, follow the immutable rule by Lord Brahma of moving these disk one at a time.

Hence this puzzle is often called Tower of Brahma puzzle.

Tower of Hanoi is one of the classic problems to look at if you want to learn recursion.

It is good to understand how recursive solutions are arrived at and how parameters for this recursion are implemented.

## Rules 
The game consists of **n** towers with **m** disks placed on over the other.

### Objective
Move the stack from one tower to another tower.

### Constraints
1. Only one disk can be moved at a time
2. No disk can be placed on top of the smaller disk

## References
- [Wiki - Tower of Hanoi](https://en.wikipedia.org/wiki/Tower_of_Hanoi)
- [Tower of Hanoi recursion game algorithm explained](https://www.hackerearth.com/blog/developers/tower-hanoi-recursion-game-algorithm-explained/)